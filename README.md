
# NGS data analysis on command line

This hands-on course will teach bioinformatic approaches for analyzing Illumina sequencing data. Our goal is to introduce the command line skills you need to make the most of your NGS data.  
During this 4-day training we will first introduce the Linux environment, shell commands and basic R scripting. And then we will focus on two NGS data analyses — small RNA-seq and RNA-seq — based on published datasets from the model organism Arabidopsis thaliana.  

[Workshop schedule](Workshop_schedule.md)


