
# Unix for biologists

### Workshop : NGS data analysis on the command line
### Valérie Cognat

<br/>


## Why Unix?

[Unix] is an operating system which was first developed in the 1970s. It is a stable, multi-user, multi-tasking system for servers, desktops and laptops. There are several variants of Unix (including [Linux] or [Mac OS X]), though the differences do not matter much for most basic functions.

[Unix]: http://en.wikipedia.org/wiki/Unix
[Linux]: http://en.wikipedia.org/wiki/Linux
[Mac OS X]: https://en.wikipedia.org/wiki/MacOS


Much of the bioinformatics software is installed on computer that have the unix operating system.

The aims of this course is to give you general commands to work on unix system.


## The Terminal ##

A **terminal** is the program that allows you to instruct computer to do a task (to view files, run programs etc). All Unix machines will have a terminal program available.

When you open the terminal application, you will always be in your *home* directory (containing your own files or directories).  


## Unix command line ##

We use the command line to type the name of the command to execute.
Commands are interpreted by a program referred to as shell – an interface between Unix and the user. We use the shell called bash.
Typically, each command is typed in one line and “entered” by hitting the Enter key on the keyboard.
Commands deal with files and processes, e.g.:
- request information (e.g., list files in a directory)
- launch a simple task (e.g., rename a file)
- run a bioinformatics tool (e.g., bowtie aligner, IGV viewer, ...)
- stop an application

During this course, we will see the most useful commands for linux. You will learn to move from a directory to an other, to read a file, to create file/directory, ...  You can find lot of documentation and tutorial on the Web. You will find the summary in this [cheat sheet] !  
[cheat sheet]: fwunixref.pdf
![cheatsheet](CheatSheet-Unix.png)

Be careful : Everything in Unix is case-sensitive !
