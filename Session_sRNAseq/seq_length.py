from Bio import SeqIO
import sys
filename = sys.argv[1]

total = 0

for seq_record in SeqIO.parse(filename, "fasta"):
    print seq_record.id + ", length " + str(len(seq_record.seq)) + " bp"
    total = total + len(seq_record.seq)

print "---------------------------"
print "Total size: " + str(total) + " bp"
