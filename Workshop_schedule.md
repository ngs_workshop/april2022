# Workshop: NGS data analysis on the command line  

**IBMP, Strasbourg - 31 March; 1, 4, 5 April 2022**

### Overview

During this 4-day workshop we will introduce you to the Linux environment, to shell commands and to basic R scripting. Using these **command-line skills (Session 1)** and the **R language (Session 2)**, we will then embark on two NGS data analyses -- **small RNA-seq (Day 3)** and **RNA-seq (Day 4)** -- based on published datasets from the model organism *Arabidopsis thaliana*.

### Instructors

* (Da)vid Pflieger
* (Ca)lvin Matteoli
* (Hé)lène Zuber
* (Ja)ckson Peter
* (Sa)ndrine Koechler
* (Ma)lek Alioua
* (St)éfanie Graindorge
* (To)dd Blevins
* (Va)lérie Cognat

## Locations

All trainings will be held in room Léon Hirth.

## Requirements

A laptop with at least 8Go RAM and 100Go free space on the hard drive.
[VirtualBox](https://www.virtualbox.org/) in order to load the CentOS7 Virtual Machine.

## Schedule

⚠️ Lunch is not provided but there will be free coffee available.

### Session 1 (Thursday, March 31st) - Unix for biologists

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 14:00 | **Welcome and Introduction to Unix environment** | Va + Da + St |
| 14:15 | **Introduction to command line (bash)** | Va + Da + St |
| 16:00 | Afternoon Break | |
| 18:00 | Close Session 1 | |

### Session 2 (Friday, April 1st) - Introduction to R

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:30 | **Introduction to R: Core Language Tutorial** | Da + Ja + Ca + Va |
| 10:30 | Coffee Break | |
| 11:00 | **Data processing** | Da + Ja + Ca + Va |
| 12:30 | Lunch, *on your own* | |
| 14:00 | **Data visualization** | Da + Ja + Ca + Va |
| 15:30 | Afternoon Break | |
| 16:00 | **Statistical tests in R** | Da + Ja + Ca + Va |
| 17:00 | Close Session 2 | |

### Session 3 (Monday, April 4th) - NGS and small RNA-seq fundamentals

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to Next-Generation Sequencing (NGS) technologies** | Sa + Ma |
| 10:30 | Morning Break | |
| 11:00 | **small RNA-seq data quality control & trimming** | Da + To + Ca |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **small RNA-seq alignment to reference genome** | Da + To + Ca |
| 15:00 | Afternoon Break | |
| 15:30 | **small RNA-seq visualization and analysis (JBrowse, R and ggplot2)**| Da + To + Ca |
| 17h30 | Close Session 3 | |

### Session 4 (Tuesday, April 5th) - RNA-seq analysis

| **Time** | **Topic** | **Instructors** |
| -------- | --------- | --------------- |
| 09:00 | **Introduction to RNA-seq data acquisition and preprocessing of RNA-seq reads** | Hé + St + Da |
| 10:30 | Morning Break | |
| 11:00 | **Mapping of RNA-seq data and preparing files for Differential Expression analysis** | Hé + St + Da |
| 12:00 | Lunch, *on your own* | |
| 13:30 | **Analysis of *A.thaliana* RNA-seq data** | Hé + St + Da |
| 15:30 | Afternoon Break | |
| 16:00 | **IGV and visualization tools** | Hé + St + Da |
| 17:30 | Close Session 4 | |
